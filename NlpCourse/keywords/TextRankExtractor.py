# -*- coding:utf-8 -*-
'''
Created on May 18, 2019

@author: Jackie
'''

import jieba
import jieba.posseg
import jieba.analyse

path = "1.txt"
file = open(path,"r",encoding="UTF-8")

text=""
for line in file:
    text= text+"\n"+line

# jieba.analyse.textrank(text, topK=20, withWeight=False, allowPOS=('ns', 'n', 'vn', 'v'))

for x, w in jieba.analyse.textrank(text, topK=20, withWeight=False, allowPOS=('ns', 'n', 'vn', 'v')):
    print('%s %s' % (x, w))
