## 第二章 自动分词标注

### 中文分词原理
中文分词(Chinese Word Segmentation) 指的是将一个汉字序列切分成一个一个单独的词。分词就是将连续的字序列按照一定的规范重新组合成词序列的过程。

现有的分词方法可分为两大类：基于字符串匹配的分词方法和基于统计的分词方法。

此外，还有基于理解的分词方法，通过让计算机模拟人对句子的理解，达到识别词的效果。其基本思想就是在分词的同时进行句法、语义分析，利用句法信息和语义信息来处理歧义现象。由于汉语语言知识的笼统、复杂性，难以将各种语言信息组织成机器可直接读取的形式，因此目前基于理解的分词系统尚无可用的工具。
#### 基于字符串匹配的方法
基于字符串匹配的方法中比较典型的算法是**正向最大匹配**算法和**逆向最大匹配**算法
```python
#!/usr/bin/python3


def HelloWorld():
	print("HelloWorld!")


def load_Dictionary():
	dic={}
	path = "data/usr.dic"
	file = open(path,"r",encoding="UTF-8")
	for line in file:
		array = line.strip().split("\t")
		print(array)
		attr ={}
		attr["natrue"]=array[2]
		attr["freq"]= array[1]
		word = array[0]
		dic[word]=attr
	print(dic)
	return dic

#sline是要切分的字符串
def FMM(wordDict, sline):
	maxLen=4
	wordList = []#用作存储已切分的词
	slineLen = len(sline)#待切分字符串的长度
	while slineLen > 0:#待切分字符串长度大于0
		if (slineLen > maxLen):
			wordLen = maxLen
		else:
			wordLen = slineLen
		subStr = sline[0:wordLen]
		while wordLen > 1:#截取的字符串长度大于1
			if (subStr in wordDict):
				break
			else:
				wordLen = wordLen - 1
				subStr = subStr[0:wordLen]		
		wordList.append(subStr)
		print(subStr)
		sline = sline[wordLen:]
		slineLen = slineLen - wordLen

	print(wordList)
	return wordList


def RMM(wordDict, sline):
	maxLen=3
	wordList = []#用作存储已切分的词
	slineLen = len(sline)#待切分字符串的长度
	while slineLen > 0:#待切分字符串长度大于0
		if (slineLen > maxLen):
			wordLen = maxLen
		else:
			wordLen = slineLen
		subStr = sline[slineLen-wordLen:]
		while wordLen > 1:#截取的字符串长度大于1
			if (subStr in wordDict):
				break
			else:
				wordLen = wordLen - 1
				subStr = subStr[1:]		
		wordList.append(subStr)
		print(subStr)
		sline = sline[0:slineLen-wordLen]
		slineLen = slineLen - wordLen
	wordList.reverse()
	# print(wordList)
	return wordList

def tag(wordDict, wordList):
	wlist =[]
	for word in wordList:
		if( word in wordDict):
			value = wordDict[word]
			natrue = value["natrue"]
			wlist.append("%s/%s"%(word,natrue))
		else:
			natrue= "x"
			wlist.append("%s/%s"%(word,natrue))
	return wlist

#入口函数
if __name__ == '__main__':
	#HelloWorld()
	usrdic= load_Dictionary()
	# print(usrdic)
	line = "今天天气不错"
	# FMM(usrdic,line)
	wordList =RMM(usrdic,line)

	wordList = tag(usrdic,wordList)
	resultLine = " ".join(wordList)
	print(resultLine)
```
#### 基于统计的分词方法

基于统计的分词方法是在给定大量已经分词的文本的前提下，利用统计机器学习模型学习词语切分的规律（称为训练），从而实现对未知文本的切分。例如最大概率分词方法和最大熵分词方法等。随着大规模语料库的建立，统计机器学习方法的研究和发展，基于统计的中文分词方法渐渐成为了主流方法

主要的统计模型有：N元文法模型（N-gram），隐马尔可夫模型（Hidden Markov Model，HMM），最大熵模型（ME），条件随机场模型（Conditional Random Fields，CRF）等。

在实际的应用中，基于统计的分词系统都需要使用分词词典来进行字符串匹配分词，同时使用统计方法识别一些新词，即将字符串频率统计和字符串匹配结合起来，既发挥匹配分词切分速度快、效率高的特点，又利用了无词典分词结合上下文识别生词、自动消除歧义的优点。

### 中文分词工具
国内比较流行的中文分词工具，如jieba、SnowNLP、THULAC、NLPIR，上述分词工具均已在github上开源。
#### Jieba安装
使用pip安装Jieba即可，控制台命令如下：
```
pip install jieba
```

#### Jieba分词样例
```python
#!/usr/bin/python3
"""
jieba分词测试
"""

import jieba

#全模式
test1 = jieba.cut("今天天气不错！", cut_all=True)
print("全模式: " + "| ".join(test1))

#精确模式
test2 = jieba.cut("今天天气不错！", cut_all=False)
print("精确模式: " + "| ".join(test2))

#搜索引擎模式
test3= jieba.cut_for_search("今天天气不错！")  
print("搜索引擎模式:" + "| ".join(test3))
```

#### Jieba词性标注
```python
#!/usr/bin/python3
import jieba.posseg as pseg

words = pseg.cut("我爱北京天安门")
for word, flag in words:
    print('%s %s' % (word, flag))
```

Jieba加载用户词典、停用词并将分词结果写入文本
```python
#!/usr/bin/python  
#-*- encoding:utf-8 -*-  
import jieba                                           #导入jieba模块
import re 
jieba.load_userdict("usr.dic")                     #加载自定义词典  
import jieba.posseg as pseg 

def splitSentence(inputFile, outputFile):
    #把停用词做成字典
    stopwords = {}
    fstop = open('stop_words.txt', 'r')
    for eachWord in fstop:
        stopwords[eachWord.strip().decode('utf-8', 'ignore')] = eachWord.strip().decode('utf-8', 'ignore')
    fstop.close()

    fin = open(inputFile, 'r')                                  #以读的方式打开文件  
    fout = open(outputFile, 'w')                                #以写得方式打开文件  
    jieba.enable_parallel(4)                                    #并行分词
    for eachLine in fin:
        line = eachLine.strip().decode('utf-8', 'ignore')       #去除每行首尾可能出现的空格，并转为Unicode进行处理 
        line1 = re.sub("[0-9\s+\.\!\/_,$%^*()?;；:-【】+\"\']+|[+——！，;:。？、~@#￥%……&*（）]+".decode("utf8"), "".decode("utf8"),line)
        wordList = list(jieba.cut(line1))                        #用结巴分词，对每行内容进行分词  
        outStr = ''  
        for word in wordList:
            if word not in stopwords:  
                outStr += word  
                outStr += ' '  
        fout.write(outStr.strip().encode('utf-8') + '\n')       #将分词好的结果写入到输出文件
    fin.close()  
    fout.close()  
  
splitSentence('ss.txt', 'tt.txt')
```